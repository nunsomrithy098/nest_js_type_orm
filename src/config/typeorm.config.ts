import { TypeOrmModuleOptions } from '@nestjs/typeorm';
export const typeOrmConfig: TypeOrmModuleOptions = {
  type: 'postgres',
  host: 'demo-postgres.cbaasywiaayh.ap-southeast-2.rds.amazonaws.com',
  port: 5432,
  username: 'postgres',
  password: 'Happylife-12',
  database: 'quiz_management',
  entities: [__dirname + '/../**/*.entity{.ts,.js}'],
  ssl: {
    rejectUnauthorized: false, // For development or testing only; for production, set up proper SSL handling
  },
  synchronize: true,
};
