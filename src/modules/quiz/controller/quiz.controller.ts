import {
  Body,
  Controller,
  Get,
  HttpCode,
  Post,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { QuizService } from '../service/quiz.service';
import { CreteQuizDto } from '../dto/create-quiz.dto';

@Controller('quiz')
export class QuizController {
  constructor(private readonly quizService: QuizService) {}

  @Get('/')
  getAllQuiz() {
    return 'Hello World';
  }

  @Post()
  @HttpCode(200)
  @UsePipes(ValidationPipe)
  async createQuiz(@Body() createQuizDto: CreteQuizDto) {
    return await this.quizService.createQuiz(createQuizDto);
  }
}
