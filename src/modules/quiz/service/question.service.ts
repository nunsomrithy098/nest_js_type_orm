import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateQuestionDto } from '../dto/create-question.dto';
import { Question } from '../entity/question.entity';
import { QuestionRepository } from '../repository/question.repository';

@Injectable()
export class QuestionService {
  constructor(
    @InjectRepository(Question) private questionRepository: QuestionRepository,
  ) {}

  async createQuestion(
    createQuestionDto: CreateQuestionDto,
  ): Promise<Question> {
    return await this.questionRepository.save(createQuestionDto);
  }
}
