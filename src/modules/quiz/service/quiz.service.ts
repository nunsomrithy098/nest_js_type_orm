import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { QuizRepository } from '../repository/quiz.repository';
import { CreteQuizDto } from '../dto/create-quiz.dto';
import { Quiz } from '../entity/quiz.entity';

@Injectable()
export class QuizService {
  constructor(@InjectRepository(Quiz) private quizRepository: QuizRepository) {}

  async createQuiz(createQuizDto: CreteQuizDto) {
    return await this.quizRepository.save(createQuizDto);
  }
}
