import { Repository } from 'typeorm';
import { Quiz } from '../entity/quiz.entity';

export class QuizRepository extends Repository<Quiz> {}
