import { Repository } from 'typeorm';
import { Question } from '../entity/question.entity';

export class QuestionRepository extends Repository<Question> {}
