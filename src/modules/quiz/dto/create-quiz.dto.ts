import { IsNotEmpty, Length } from 'class-validator';

export class CreteQuizDto {
  @IsNotEmpty({ message: 'The quiz must have a  title' })
  @Length(3, 255)
  title: string;

  @IsNotEmpty()
  @Length(3)
  description: string;
}
